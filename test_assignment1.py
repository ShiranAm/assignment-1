import urllib.request
import urllib.error
import re

def test_1():
    html_content = urllib.request.urlopen('https://the-internet.herokuapp.com/context_menu').read()
    matches = re.search(r"<p>Right-click in the box below to see one called \'the-internet\'", html_content.decode("utf-8"))
    assert matches is not None, "FAILED: string ' Right-click in the box below to see one called 'the-internet' ' not found" 

def test_2():
    html_content = urllib.request.urlopen('https://the-internet.herokuapp.com/context_menu').read()
    matches = re.search("Alibaba", html_content.decode("utf-8"))
    assert matches is not None, "FAILED: string 'Alibaba' not found" 
